package ru.tsc.marfin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.marfin.tm.util.TerminalUtil;

import java.util.Date;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-create";

    @NotNull
    public static final String DESCRIPTION = "Create new project";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        System.out.println("ENTER START DATE:");
        @Nullable final Date dateStart = TerminalUtil.nextDate();
        System.out.println("ENTER END DATE:");
        @Nullable final Date dateEnd = TerminalUtil.nextDate();
        @NotNull final String userId = getUserId();
        getProjectService().create(userId, name, description, dateStart, dateEnd);
    }

}
