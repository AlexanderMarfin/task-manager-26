package ru.tsc.marfin.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.marfin.tm.comparator.CreatedComparator;
import ru.tsc.marfin.tm.comparator.DateStartComparator;
import ru.tsc.marfin.tm.comparator.NameComparator;
import ru.tsc.marfin.tm.comparator.StatusComparator;

import java.util.Comparator;

@Getter
public enum Sort {

    BY_NAME("Sort by name", NameComparator.INSTANCE),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE),
    BY_CREATED("Sort by created", CreatedComparator.INSTANCE),
    BY_DATE_START("Sort by start date", DateStartComparator.INSTANCE);

    @Nullable
    public static Sort toSort(@NotNull final String value) {
        if (value.isEmpty()) return null;
        for (@NotNull final Sort sort : values()) {
            if(sort.name().equals(value)) return sort;
        }
        return null;
    }

    @NotNull
    private final String displayName;

    @NotNull
    private final Comparator comparator;

    Sort(@NotNull final String displayName, @NotNull final Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

}
