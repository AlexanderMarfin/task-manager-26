package ru.tsc.marfin.tm.exception.entity;

import org.jetbrains.annotations.NotNull;
import ru.tsc.marfin.tm.exception.AbstractException;

public abstract class AbstractEntityNotFoundException extends AbstractException {

    public AbstractEntityNotFoundException() {
    }

    public AbstractEntityNotFoundException(@NotNull final String message) {
        super(message);
    }

    public AbstractEntityNotFoundException(@NotNull final String message, @NotNull final Throwable cause) {
        super(message, cause);
    }

    public AbstractEntityNotFoundException(@NotNull final Throwable cause) {
        super(cause);
    }

    public AbstractEntityNotFoundException(
            @NotNull final String message,
            @NotNull final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
